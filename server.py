from flask import Flask, jsonify, send_file, request
app = Flask(__name__)
app.url_map.strict_slashes = False

import os
import hashlib

DIR = "Data"

@app.route("/")
def index():
    return "Data Synchronization Server"

@app.route("/ping")
def ping():
    return "pong"

@app.route("/file-list")
def fileList():
	file_list = []
	for path, subdirs, files in os.walk(DIR):
		for name in files:
			file = os.path.join(path, name)
			md5 = hashlib.md5(open(file,'rb').read()).hexdigest()
			
			file_list.append({
				"path": file,
				"md5": md5
			})

	return jsonify({
		"dir": DIR,
		"files": file_list
	})

@app.route('/file-get/', methods=['GET'])
def fileGet():
	path = request.args.get('path', '')

	if path and path.startswith(DIR) and os.path.isfile(path):
		return send_file(path)

	return "File Does Not Exist", 444
